# vagrant-ruby-2-and-rails

Made to scratch my own itch.

I want to install a bleeding edge Ruby dev environment using vagrant.

I'm using vagrant's fabric plugin to:

* install `ruby 2.1.0` using `rbenv` and set that version as the global one
* setup any kind of database as well

## Dependencies

### Python

    $ sudo apt-get install python-setuptools python-dev python-pip

**note:** this would only work in a debian-flavored linux box. Replace `apt-get`
with whatever your system is using as a package manager.

### Fabric

    $ sudo pip install fabric

### Vagrant's fabric plugin

    $ cd vagrant
    $ vagrant plugin install vagrant-fabric

You're now good to use `vagrant up` as usual !